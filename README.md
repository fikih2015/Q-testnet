<p align="center">
  <img width="300" height="auto" src="https://user-images.githubusercontent.com/108969749/201537323-e380f751-fdf5-4e2a-98c7-7139664e2df3.png">
</p>

### Spesifikasi Hardware :
NODE  | CPU     | RAM      | SSD     |
| ------------- | ------------- | ------------- | -------- |
| Testnet | 4          | 8         | 160  |

### Install docker-compose
```
sudo apt update && sudo apt upgrade -y
sudo apt install docker-compose -y
```

### Download program node
```
git clone https://gitlab.com/q-dev/testnet-public-tools
cd testnet-public-tools/testnet-validator
```
```
docker-compose up
```
### Hasilkan pasangan kunci untuk validator
  * membuat password untuk keystore
```
mkdir data
```
```
cd data
```
```
nano pwd.txt
```
```
cd ..
```
  
* menghasilkan keystore otomatis
```
docker run --entrypoint="" --rm -v $PWD:/data -it qblockchain/q-client:testnet geth account new --datadir=/data --password=/data/pwd.txt
```
Output
> Your new key was generated
> Public address of the key:   0xb3FF24F818b0ff6Cc50de951bcB8f86b52287dac
> Path of the secret key file: /data/UTC--2021-01-18T11-36-28.705754426Z--b3ff24f818b0ff6cc50de951bcb8f86b52287dac

### Mendapatkan token Q
[Go to Faucet](https://faucet.qtestnet.org/)

### Konfigurasi node
Address tidak menggunakan 0X
 * edit .env
```
nano .env
```
lihat hasil :
> ADDRESS=b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc
IP=193.19.228.94

  * edit config.json
```
nano config.json
```
lihat hasil :
> "address": "b3FF24F818b0ff6Cc50de951bcB8f86b52287DAc",
  "password": "supersecurepassword",
### Staking 
```
docker run --rm -v $PWD:/data -v $PWD/config.json:/build/config.json qblockchain/js-interface:testnet validators.js
```
### Menambhakan validator ke consensus
```
nano docker-compose.yaml
```
* membuat nama validator

  <i>pastikan sehabis tanda [,] adalah space</i>
```
image: $QCLIENT_IMAGE
  entrypoint: ["geth", "--ethstats=<nama nodemu>:qstats-testnet@stats.qtestnet.org", 
```
lihat hasil :
> ["geth", "--ethstats=whalealert:qstats-testnet@stats.qtestnet.org", "--datadir=/data", "--nat=extip:$IP",...

### Run node
```
docker-compose up -d
```
 * cek log node
```
docker-compose logs -f --tail "100"
```
 * stop node
```
docker compose down
```
